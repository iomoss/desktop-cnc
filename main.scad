include <config/config.scad>
/*
// Translate from the leadscrew to the axis
y_axis_leadscrew_x_translate = 0;
y_axis_leadscrew_y_translate = 0;
y_axis_leadscrew_z_translate = 0;
*/
//-----------//
// RENDERING //
//-----------//
//x_axis();
y_axis(x_offset = 0, y_offset = 0, z_offset = 0);
//z_axis();

// Footprint displacement
%cube([x_footprint,y_footprint,1], center = true);

// Work area
%translate([0, -40, 2]) cube([100,100,1], center = true);

module x_axis() {
// X axis
//for(y = [-1,1]) translate([0, y*20, y*20+60]) rotate([0,90,0]) cylinder(h = 200, r = 8/2, center = true);
}

module y_idler_bottom_block(x = 1)
{
    difference()
    {
        hull()
        {
            // Motor wide bottom
            cube([y_motor_size,y_motor_size/2,0.1], center=true);
            translate([0, 0, y_motor_size/2])
            rotate([90,0,0])
            cylinder(r=12, h=y_motor_size/2, center=true);
        }
        translate([0, 0, y_motor_size/2])
            rotate([90,0,0])
            cylinder(r=y_axis_leadscrew_d/2, h=y_motor_size, center=true);

        translate([0, y_motor_size/4, y_motor_size/2])
            rotate([90,0,0])
            cylinder(r=12/2, h=y_motor_size/4, center=true);
    }

    // Smooth rod
    translate([x*y_motor_size/2-x*(min_wall+y_axis_bearing_od/2),0,0])
    {
        difference()
        {
            hull()
            {
                cube([(min_wall+y_axis_bearing_od/2)*2, y_motor_size/2, 0.1], center=true);
                translate([0,0,y_motor_size/2+y_axis_coupling_d/2+y_axis_bearing_od/2+min_wall])
                    cube([(min_wall+y_axis_bearing_od/2)*2, y_motor_size/2, 0.1], center=true);
            }

            translate([0,0,y_motor_size/2+y_axis_coupling_d/2+y_axis_bearing_od/2+min_wall])
                rotate([90,0,0])
                cylinder(r=8/2, h=y_motor_size, center=true);

            for(i=[-1,1])
            translate([-16/2*i,0,y_motor_size/2+y_axis_coupling_d/2+y_axis_bearing_od/2+min_wall])
            {
                cylinder(r=3/2, h=20, center=true, $fn=25);
                hull()
                for(j=[0,1])
                translate([10*-j*i,0,-9+2/2])
                cylinder(r=5/2, h=2, center=true, $fn=6);
            }
        }
    }
}

module y_idler_top_block()
{
    difference()
    {
        rotate([90,0,0])
            cylinder(r=min_wall+y_axis_bearing_od/2, h=y_motor_size/2, center=true);

        translate([0,0,-16/2])
            cube([(min_wall+y_axis_bearing_od/2)*2+5, y_motor_size/2+5, 16], center=true);

        rotate([90,0,0])
            cylinder(r=8/2, h=y_motor_size, center=true);

        for(i=[-1,1])
        translate([-16/2*i,0,0])
        {
            cylinder(r=3/2, h=100, center=true, $fn=25);
            translate([0,0,10])
                cylinder(r=5/2, h=10, center=true, $fn=25);
        }
    }
}

module y_idler_block(x = 1)
{
    y_idler_bottom_block(x);

    translate([x*y_motor_size/2-x*(min_wall+y_axis_bearing_od/2),0,y_motor_size/2+y_axis_coupling_d/2+y_axis_bearing_od/2+min_wall])
        y_idler_top_block();

}

module y_axis_part(x)
{
    translate([x*(-y_motor_size)/2,(y_footprint/2)-y_motor_l,y_motor_size/2])
        rotate([90,45*x+45,0])
        y_motor();
/*
    %translate([0, -(y_motor_l)/2, y_motor_size/2+y_axis_coupling_d/2+y_axis_bearing_od/2+min_wall])
        translate([x*(-y_axis_bearing_od-(min_wall*2))/2,0,0])
        rotate([90,0,0])
        {
            color("lightgray")
            cylinder(h = y_footprint - (y_motor_l), r = y_axis_d/2, center = true);
            color("gray")
            cylinder(h = y_axis_bearing_l, r= y_axis_bearing_od/2, center=true);
        }
*/
    // Y lead screws
    %color("gray")
    translate([x*(-y_motor_size)/2, -(y_motor_l+y_motor_axis_l)/2, y_motor_size/2])
        rotate([90,0,0])
        cylinder(h = y_footprint - (y_motor_l+y_motor_axis_l), r = y_axis_leadscrew_d/2, center = true);

    translate([x*(-y_motor_size)/2,-y_footprint/2+y_motor_size/4,0])
        y_idler_block(x);
}

module y_axis(x_offset = 0, y_offset = 0, z_offset = 0) {
// Y motors

    for(x = [-1,1])
        translate([x*(x_footprint/2),0,0])
            y_axis_part(x);
}

module z_axis() {

}

module y_motor()
{
        motor(motor_size = y_motor_size, motor_l = y_motor_l, nema = true, motor_axis_d = y_motor_axis_d, motor_axis_l = y_motor_axis_l, motor_mount_holes_offset = y_motor_mount_holes_offset, motor_mount_holes_d = y_motor_mount_holes_d);
}

module motor(motor_size = 42, motor_l = 38, nema = true, motor_axis_d = 5, motor_axis_l = 20, motor_mount_holes_offset = sqrt(15.5*15.5+15.5*15.5), motor_mount_holes_d = 3) {
	difference() {
		union() {
			if(nema == true) {
				translate([0,0,-motor_l/2]) cube([motor_size,motor_size,motor_l], center=true);
				cylinder(r = 21/2, h = 2);
			} else {
				translate([0,0,-motor_l/2]) cylinder(r = motor_size/2, h = motor_l, center=true);
				hull() {
					for(i = [-1,1]) rotate(45) translate([i*motor_mount_holes_offset,0,-1]) cylinder(r = motor_mount_holes_d, h = 2, center=true);
					translate([0,0,-1]) cylinder(r = motor_size/2, h = 2, center=true);
				}
			}
			translate([0,0,motor_axis_l/2]) cylinder(r = motor_axis_d/2, h = motor_axis_l, center=true);
		}
		union() {
			if(nema == true) {
				#for(i = [0:3]) rotate( i * 360 / 4+45, [0, 0, 1]) translate([motor_mount_holes_offset,0,-1.5]) cylinder(r = motor_mount_holes_d/2, h = 3.1, center=true);
			} else {
				#for(i = [0:1]) rotate( i * 360 / 2+45, [0, 0, 1]) translate([motor_mount_holes_offset,0,-1]) cylinder(r = motor_mount_holes_d/2, h = 2.2, center=true);
			}
		}
	}
}
