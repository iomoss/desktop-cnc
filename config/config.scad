// Desktop-cnc
// INFO: Unit is in millimeters

min_wall = 3;

x_footprint = 200;
y_footprint = 200;

x_motor_axis_d = 5;
x_motor_axis_l = 20;
x_motor_l = 38;
x_motor_mount_holes_d = 3;
x_motor_mount_holes_offset = sqrt(15.5*15.5+15.5*15.5);
x_motor_nema = true;
x_motor_size = 42;

y_motor_axis_d = 5;
y_motor_axis_l = 20;
y_motor_l = 38;
y_motor_mount_holes_d = 3;
y_motor_mount_holes_offset = sqrt(15.5*15.5+15.5*15.5);
y_motor_nema = true;
y_motor_size = 42;

z_motor_axis_d = 5;
z_motor_axis_l = 20;
z_motor_l = 38;
z_motor_mount_holes_d = 3;
z_motor_mount_holes_offset = sqrt(15.5*15.5+15.5*15.5);
z_motor_nema = true;
z_motor_size = 42;

x_axis_bearing_l = 16;
x_axis_bearing_od = 16;
x_axis_d = 8;
x_axis_leadscrew_bearing_h = 16;
x_axis_leadscrew_bearing_od = 16;
x_axis_leadscrew_d = 6;
x_axis_coupling_d = 20;

y_axis_bearing_l = 16;
y_axis_bearing_od = 16;
y_axis_d = 8;
y_axis_leadscrew_bearing_h = 16;
y_axis_leadscrew_bearing_od = 16;
y_axis_leadscrew_d = 6;
y_axis_coupling_d = 20;

z_axis_bearing_l = 16;
z_axis_bearing_od = 16;
z_axis_d = 8;
z_axis_leadscrew_bearing_h = 16;
z_axis_leadscrew_bearing_od = 16;
z_axis_leadscrew_d = 6;
z_axis_coupling_d = 20;
